var time;

function addZero(n, needLength) {
  needLength = needLength || 2;
  n = String(n);
  while (n.length < needLength) {
    n = "0" + n;
  }
  return n
}

setInterval(function() {
	time = new Date();
	document.querySelector('#time').innerHTML = addZero(time.getHours()) + ":" + addZero(time.getMinutes());
},1000)